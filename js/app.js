var $ = Dom7;

var app = new Framework7({
	root: '#app',
	name: 'DOT',
	id: 'com.renz.salanga',
	theme: 'auto',
	touch: {
		fastClicks: true,
	},
	routes: routes,
	data: function(){
		return{
			userdata: null
		}
	},
	view: {
		mdPageLoadDelay: 500
	},
	on: {
		pageInit: function(page){
			var self = this;
			
			if(page.name == "home"){
				$('#login-btn').on('click',function(){
					app.dialog.preloader("Logging in..");
					setTimeout(function () {
						self.data.userdata = {
							username: 'kingrenz23',
							name: 'Renz Carlo Salanga'
						}
						app.dialog.close();
						mainView.router.navigate("/home/");
					}, 1000);
				});
			}
		}
	}
});

var mainView = app.views.create('.view-main');