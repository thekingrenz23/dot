var wire = {
	post: function(url,data, callback, app){
		var request = new XMLHttpRequest();
		
        request.onreadystatechange = function(){
            if(this.readyState == 4 && this.status == 200){
                callback(this.responseText);
			}else if(this.readyState == 4){
				app.dialog.alert(`
					Error loading.
					Please Check your internet connection
				`, function(){
					request.open("POST", url, true);
					request.send(JSON.stringify(data));
				});
            }
        }

        request.open("POST", url, true);
        
        request.send(JSON.stringify(data));
	}
}