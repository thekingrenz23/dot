var routes = [
	{
		path: '/',
		url: './index.html'
	},
	{
		path: '/home/',
		componentUrl: './pages/home.html'
	},
	{
		path: '/attractions/',
		componentUrl: './pages/attractions.html'
	},
	{
		path: '/view_attraction/:id',
		componentUrl: './pages/view_attraction.html'
	},
	{
		name: 'map',
		path: '/direction/:dlat/:dlng/:ulat/:ulng',
		componentUrl: './pages/direction.html'
	},
	{
		path: '(.*)',
		url: './pages/404.html'
	}
];