function Direction(userlocation, destination){
	/*
	*
	*Locations Data
	*
	*/
	this.userlocation = userlocation;
	this.destination = destination;
	
	/*
	*
	*Element, this is where the map will be attached
	*
	*/
	this.map = null;
}


/*
*
*Attach map function use to display the map to the DOM
*
*/
Direction.prototype.attachMap = function(element){
	this.map = L.map(element, { zoomControl:false }).setView([this.destination.lat, this.destination.lng], 13);
	
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWhvcnRpenVlbGEiLCJhIjoiY2p4bXMyNGxsMDJ0MjNobnY5c2Q1ZmJtdSJ9.ewQ3i3GuaP9k7-R0oNPB7Q', {
		maxZoom: 19,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			 '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			 'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		 id: 'mapbox.streets'
	}).addTo(this.map);
}

/*
*
*Show Function use to show the direction
*
*/
Direction.prototype.showDirection = function(success, fail){
	var userIcon = L.icon({
		iconUrl: 'img/placeholder.png',
		iconSize: [45, 45],
	});
	this.userMarker = L.marker([this.userlocation.lat, this.userlocation.lng],{icon: userIcon}).addTo(this.map);
	this.destMarker = L.marker([this.destination.lat, this.destination.lng],{}).addTo(this.map);
	
	this.route = L.Routing.control({
		router: new L.Routing.openrouteservice('5b3ce3597851110001cf6248cf14426437a04a62802b532c39cf8b44'),
		waypoints: [
			L.latLng(this.userlocation.lat, this.userlocation.lng),
			L.latLng(this.destination.lat, this.destination.lng)
		],
		createMarker: function() { return null; },
		addWaypoints: false,
		draggableWaypoints: false, 
		show: false
	}).addTo(this.map);
	
	this.route.addEventListener('routingerror', fail);
	this.route.addEventListener('routesfound', success);
}